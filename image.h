#ifndef IMAGE_H
#define IMAGE_H

#include <vector>
#include <cmath>

#include <iostream>

namespace BmpLib {
struct vec3b
{
    union {
        unsigned char x;
        unsigned char r;
        unsigned char h;
    };

    union {
        unsigned char y;
        unsigned char g;
        unsigned char s;
    };

    union {
        unsigned char z;
        unsigned char b;
        unsigned char v;
    };
};

class BmpLoader;

/*!
   \class Image
   \brief class for storing image data.
*/
class Image
{
private:
    const unsigned int width;
    const unsigned int height;
    std::vector<vec3b> pixels;

    Image(const unsigned int width, const unsigned int height, std::vector<vec3b> &pixels);
public:
    friend class BmpLoader;

    Image(const unsigned int width, const unsigned int height);

    uint getWidth() const;
    uint getHeight() const;

    void setPixel(const uint x, const uint y, const vec3b &color);

    const vec3b getPixel(const uint x, const uint y) const;

    const std::vector<vec3b> &getRawPixels();
};
}

#endif // IMAGE_H
